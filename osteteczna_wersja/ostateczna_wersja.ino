const float Kp = 0.1; // stała członu proporcjonalnego
const float Kd = 0; // stała członu różniczkującego
const float velocity = 5.1; //zadana prędkość w cm/s

float PWM_trans, PWM_rot = 0; // wartości PWM translacji i rotacji
float error_trans, error_rot, previous_error_trans, previous_error_rot = 0; // dla regulatora PD, błędy obecne i poprzednie
float set_trans =  0;   //zadana translacja w cm/s
float set_rot = 0;    //zadana rotacja w cm/s
float posL, posP = 0;   //dane z enkoderów w cm/s (suma)
float zad_T = 0;        //suma translacji
float zad_R = 0;        //suma rotacji
int PWM_L, PWM_P = 0;    //pwm ustawiane na robocie 

float LF_wheel, RF_wheel, LB_wheel, RB_wheel, number1,number=0;   //dane z I2C
String inString = "";                                   //string z I2C
bool sign = 0;
bool kind =0;
bool dostal=0;
bool flag = 1;
int kier=0;
//definicje poszczególnych pinów arduino
#define predkoscLewe 3
#define predkoscPrawe 11
#define kierunekLewe 12
#define kierunekPrawe 13
#define hamulecLewe 9
#define hamulecPrawe 8

#include <Wire.h>             //biblioteka do I2C
#define SLAVE_ADDRESS 0x04    //ustawienie potrzebne do I@C


void setup() {
   
   //USTAWIENIE PINÓW KANAŁU A I B JAKO WYJŚCIOWE
  
  //KANAŁ A
  pinMode(kierunekLewe, OUTPUT); //KIERUNEK
  pinMode(hamulecLewe, OUTPUT); //HAMULEC
  pinMode(predkoscLewe, OUTPUT); //PWM

  //KANAŁ B
  pinMode(kierunekPrawe, OUTPUT); //KIERUNEK
  pinMode(hamulecPrawe, OUTPUT); //HAMULEC
  pinMode(predkoscPrawe, OUTPUT); //PWM
  digitalWrite(kierunekLewe,HIGH);
  digitalWrite(kierunekPrawe,HIGH);
  delay(2000);
  stop();
  //TRANSMISJA I2C
  Serial.begin(9600);
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Serial.println("Ready!");
}

void loop() 
{
  
    //Serial.println("i2c:");
    //Serial.println(LF_wheel);
    //Serial.println(RF_wheel);
    //Serial.println(LB_wheel);
    //Serial.println(RB_wheel);
    //Serial.println(number);
    //set_rot=number*0.01*velocity;       //*0.5
    //set_trans=(velocity-(fabs)(set_rot)*0.5); 
    if (dostal==0)
    {
      stop();
    }
    else{
    Serial.println(number);
    if(number == 200)
    {
      stop();
      delay(3000);
      switch(kier)
      {
        case 0:
        ruszanie();
        break;
        case -1:
        skret_lewo();
        break;
        case 1:
        skret_prawo();
        break;
      }
      stop();
      
      //STOP
      //Serial.println("stop");
    }
    else if (number<50 && number>-50)
    {
      kier=0;
      ruszanie();
      stop();
      delay(1000);
      /*
      if(LF_wheel==0 && LB_wheel==0 && RF_wheel==0 && RB_wheel==0)
      {
        ruszanie();
        Serial.println(PWM_L);
      }
      else
      {
        PWM_L=65;
        PWM_P=65;
        analogWrite(predkoscLewe,PWM_L);  //ustawienie prędkości
        analogWrite(predkoscPrawe,PWM_P);  //
        //set_rot=0;
        //set_trans=velocity;   
        
        //delay(1000);
        //posL=(LF_wheel+LB_wheel)/2;
        //posP=(RF_wheel+RB_wheel)/2;
        //error_trans=set_trans-translation();
        //Serial.println("rot i trans:");
        //Serial.println(set_trans);
        //PWM_L+=(int)(PD_translation());
        //PWM_P+=(int)(PD_translation());
        
        //PWM_generator();
      }*/
    }
    else if (number<=-50)
    {
      kier=-1;
      stop();
      delay(3000);
      skret_lewo();
      stop();
    }
    else if (number>=50)
    {
      kier=1;
      stop();
      delay(3000);
      skret_prawo();
      stop();
    }
    } 
}
void receiveData(int byteCount)
{
  
  int inChar;
  char start = '$';
  char start2 = '#';
  
  
  while(Wire.available())
  {
    inChar = Wire.read();
    if (char(inChar) == start2)
    {
      sign=1;
      inString = "";
      kind=0;
    }
    if (char(inChar) == start){
      sign = 1;
      inString = "";
      kind = 1;
    }
    if(sign){
    inString = inString + char(inChar);
    }     
  }
  //ODBIERANIE DANYCH
  //Serial.println(inString);
  if(inString.length() >= 25)
  {
    dostal=1;
    if (kind)
    {
      LB_wheel = (inString.substring(1,6)).toFloat();
      LF_wheel = (inString.substring(6,11)).toFloat();
      RB_wheel = (inString.substring(11,16)).toFloat();
      RF_wheel = (inString.substring(16,21)).toFloat();
    }    
    number1 = (inString.substring(21,25)).toFloat();
    if(number1 == 0){
      number = number1;
      flag = 0;
    }
    if(flag){
      if(number1!=200)number=number1;
    }
    
    //Serial.print("number: ");
    //Serial.println(number1);
    inString = "";
    //Serial.println(PWM_L);
    //Serial.println(PWM_P);
  }
}
void ruszanie()
{
  flag = 1;
  digitalWrite(kierunekLewe,HIGH);
  digitalWrite(kierunekPrawe,HIGH);
  PWM_L=100;
  PWM_P=100;
  //Serial.println("zero");
  analogWrite(predkoscLewe,PWM_L);  //ustawienie prędkości
  analogWrite(predkoscPrawe,PWM_P);  //
  delay(200);
  PWM_L=65;
  PWM_P=65;
  analogWrite(predkoscLewe,PWM_L);  //ustawienie prędkości
  analogWrite(predkoscPrawe,PWM_P);  //
  delay(100);
}
void ruszanie_skret()
{
  ruszanie();
  digitalWrite(kierunekLewe,HIGH);
  digitalWrite(kierunekPrawe,HIGH);
  PWM_L=200;
  PWM_P=200;
  analogWrite(predkoscLewe,PWM_L);  //ustawienie prędkości
  analogWrite(predkoscPrawe,PWM_P);  //
  delay(120);
}
void stop()
{
  PWM_L=0;
  PWM_P=0;
  analogWrite(predkoscLewe,PWM_L);  //ustawienie prędkości
  analogWrite(predkoscPrawe,PWM_P);  //
}
void skret_lewo()
{
  ruszanie_skret();
  digitalWrite(kierunekLewe,LOW);
  digitalWrite(kierunekPrawe,HIGH);
  analogWrite(predkoscLewe,255);  //ustawienie prędkości
  analogWrite(predkoscPrawe,255);  //
  delay(130);
}
void skret_prawo()
{
  ruszanie_skret();
  digitalWrite(kierunekLewe,HIGH);
  digitalWrite(kierunekPrawe,LOW);
  analogWrite(predkoscLewe,255);  //ustawienie prędkości
  analogWrite(predkoscPrawe,255);  //
  delay(130);
}
//profiler_rotacji - sumowanie kolejnych zadanych wartości
float profiler_rot()
{
  zad_R = set_rot;
  return zad_R;
}

//profiler_translacja - sumowanie kolejnych zadanych wartości
float profiler_trans()
{
  zad_T = set_trans;
  return zad_T;
}
// translacja obliczanie
float translation()
{
  return (posL + posP)/2;
}
// rotacja obliczanie 
float rotation()
{
  return (posL - posP)/2;
}
// en_trans - translacja z enkoderow, set_trans - zadana translacja
float PD_translation()
{
  
  PWM_trans = Kp * error_trans + Kd*(error_trans - previous_error_trans);
  previous_error_trans = error_trans;

  return PWM_trans;
}
// en_rot - rotacja z enkoderow, set_rot - zadana rotacja
float PD_rotation()
{
  
  PWM_rot = Kp * error_rot + Kd*(error_rot - previous_error_rot);
  previous_error_rot = error_rot;

  return PWM_rot;
}

//generowanie sygnałów PWM, skręcanie robotem
void PWM_generator ()
{
  if (PWM_L<0)PWM_L=0;           
  if (PWM_P<0)PWM_P=0;
  if (PWM_L>255)PWM_L=255;           
  if (PWM_P>255)PWM_P=255;
  
  
  analogWrite(predkoscLewe,PWM_L);  //ustawienie prędkości
  analogWrite(predkoscPrawe,PWM_P);  //
  
}


