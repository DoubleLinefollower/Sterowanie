float number = 0;
int choice;
float speedy = 155;
float value = 0;
float ep = 0; // uchyb poprzedni
float error =0;
#define predkoscLewe 3
#define predkoscPrawe 11
#define kierunekLewe 12
#define kierunekPrawe 13
#define hamulecLewe 9
#define hamulecPrawe 8

#include <Wire.h>
#define SLAVE_ADDRESS 0x04


void setup() {
  Serial.begin(9600); 
  
   //USTAWIENIE PINÓW KANAŁU A I B JAKO WYJŚCIOWE


  //KANAŁ A
  pinMode(kierunekLewe, OUTPUT); //KIERUNEK
  pinMode(hamulecLewe, OUTPUT); //HAMULEC
  pinMode(predkoscLewe, OUTPUT); //PWM

  Serial.begin(9600);
  //KANAŁ B
  pinMode(kierunekPrawe, OUTPUT); //KIERUNEK
  pinMode(hamulecPrawe, OUTPUT); //HAMULEC
  pinMode(predkoscPrawe, OUTPUT); //PWM

  //TRANSMISJA
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Serial.println("Ready!");
}

void loop() {
  delay(100);
}


//ODBIERANIE DANYCH
void receiveData(int byteCount){

  while(Wire.available()) {
  number = Wire.read();
  if (number > 50) { //SKRĘT W PRAWO
    value =(number -50)/50 *speedy;
    choice = 0;
  }
  else if (number < -50) { // SKRĘT W LEWO
       value =-(number +50)/50 *speedy;
       choice = 1;  
  }
  else{
      if(number >= 0){
        value = (speedy *(50 - number))/50;
        choice = 2;
      }
      else{
        value = (speedy *(50 + number))/50;
        choice = 3;
      }
  }
  switch(choice){
     case 0:
      value = value - pid(error);
      digitalWrite(kierunekLewe,HIGH);  //USTAWIENIE KIERUNKU KOŁA LEWEGO W PRZÓD
      digitalWrite(kierunekPrawe,LOW);  //USTAWIENIE KIERUNKU KOŁA PRAWEGO W TYŁ
      analogWrite(predkoscPrawe,value); //PRAWE KOŁO OBLICZONA MOC 
      analogWrite(predkoscLewe,speedy);  //LEWE KOŁO PEŁNA MOC
      break;
    case 1:
      value = value - pid(error);
      digitalWrite(kierunekPrawe,HIGH);  //USTAWIENIE KIERUNKU KOŁA PRAWEGO W PRZÓD
      digitalWrite(kierunekLewe,LOW);  //USTAWIENIE KIERUNKU KOŁA LEWEGO W TYŁ
      analogWrite(predkoscLewe,value);  //LEWE KOŁO OBLICZONA MOC
      analogWrite(predkoscPrawe, speedy);  //PRAWE KOŁO PEŁNA MOC
      break;
    case 2:
      value = value - pid(error);
      digitalWrite(kierunekPrawe,HIGH);  //USTAWIENIE KIERUNKU KOŁA PRAWEGO W PRZÓD
      digitalWrite(kierunekLewe,HIGH);  //USTAWIENIE KIERUNKU KOŁA LEWEGO W PRZÓD
      analogWrite(predkoscLewe,speedy);  //LEWE KOŁO OBLICZONA MOC
      analogWrite(predkoscPrawe,value);  //PRAWE KOŁO PEŁNA MOC
      break;
    case 3:
      value = value - pid(error);
      digitalWrite(kierunekPrawe,HIGH);  //USTAWIENIE KIERUNKU KOŁA PRAWEGO W PRZÓD
      digitalWrite(kierunekLewe,HIGH);  //USTAWIENIE KIERUNKU KOŁA LEWEGO W PRZÓD
      analogWrite(predkoscLewe,value);  //LEWE KOŁO PEŁNA MOC
      analogWrite(predkoscPrawe,speedy);  //PRAWE KOŁO OBLICZONA MOC
      break;
  }
  }
}

float pid(float error){
  float dt =20; //okres próbkowania
  float en; //uchyb nastepny
  float yi; //wynik całkowania
  float yd; //wynik różniczkowania
  float U; //sygnał sterujący
  float Kp; //wzmocnienie
  float Ti; //stała całkowania
  float Td; //stała różniczkowania
  en = error; 
  yi += ((ep + en)/2)*dt;
  yd = (en - ep)/dt;
  U = Kp*(en + (1/Ti)*yi + Td*yd);
  ep = en;
  return U;
}

