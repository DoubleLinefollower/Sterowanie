const float Kp = 1; // stała członu proporcjonalnego
const float Kd = 0; // stała członu różniczkującego
const float velocity = 5; //zadana prędkość w cm/s

float PWM_trans, PWM_rot = 0; // wartości PWM translacji i rotacji
float error_trans, error_rot, previous_error_trans, previous_error_rot = 0; // dla regulatora PD, błędy obecne i poprzednie
float set_trans =  0;   //zadana translacja w cm/s
float set_rot = 0;    //zadana rotacja w cm/s
float posL, posP = 0;   //dane z enkoderów w cm/s (suma)
float zad_T = 0;        //suma translacji
float zad_R = 0;        //suma rotacji
int PWM_L, PWM_P = 0;    //pwm ustawiane na robocie 

float LF_wheel, RF_wheel, LB_wheel, RB_wheel, number;   //dane z I2C
String inString = "";                                   //string z I2C

//definicje poszczególnych pinów arduino
#define predkoscLewe 3
#define predkoscPrawe 11
#define kierunekLewe 12
#define kierunekPrawe 13
#define hamulecLewe 9
#define hamulecPrawe 8

#include <Wire.h>             //biblioteka do I2C
#define SLAVE_ADDRESS 0x04    //ustawienie potrzebne do I@C


void setup() {
   
   //USTAWIENIE PINÓW KANAŁU A I B JAKO WYJŚCIOWE
  
  //KANAŁ A
  pinMode(kierunekLewe, OUTPUT); //KIERUNEK
  pinMode(hamulecLewe, OUTPUT); //HAMULEC
  pinMode(predkoscLewe, OUTPUT); //PWM

  //KANAŁ B
  pinMode(kierunekPrawe, OUTPUT); //KIERUNEK
  pinMode(hamulecPrawe, OUTPUT); //HAMULEC
  pinMode(predkoscPrawe, OUTPUT); //PWM

  //TRANSMISJA I2C
  Serial.begin(9600);
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Serial.println("Ready!");
}

void loop() 
{
    
}
void receiveData(int byteCount)
{
  int inChar;
  
  while(Wire.available())
  {
    inChar = Wire.read();
    inString = inString + char(inChar);     
  }
  //ODBIERANIE DANYCH
  Serial.println(inString);
  if(inString.length() == 24)
  {
    LF_wheel = (inString.substring(0,5)).toFloat();
    RF_wheel = (inString.substring(5,10)).toFloat();
    LB_wheel = (inString.substring(10,15)).toFloat();
    RB_wheel = (inString.substring(15,20)).toFloat();
    number = (inString.substring(20,24)).toFloat();
    Serial.println(LF_wheel);
    Serial.println(RF_wheel);
    Serial.println(LB_wheel);
    Serial.println(RB_wheel);
    Serial.println(number);
    set_rot=number*0.01*velocity;
    set_trans=(velocity-(abs)(set_rot));
    error_trans=profiler_trans()-translation();
    error_rot=profiler_rot()-rotation();
    PWM_L=(int)(PD_translation()+PD_rotation());
    PWM_P=(int)(PD_translation()-PD_rotation());
    Serial.println(PWM_L);
    Serial.println(PWM_P);
    PWM_generator();
    delay(1000);
    posL+=(LF_wheel+LB_wheel)/2;
    posP+=(RF_wheel+RB_wheel)/2;
  }
}
//profiler_rotacji - sumowanie kolejnych zadanych wartości
float profiler_rot()
{
  zad_R += set_rot;
  return zad_R;
}

//profiler_translacja - sumowanie kolejnych zadanych wartości
float profiler_trans()
{
  zad_T += set_trans;
  return zad_T;
}
// translacja obliczanie
float translation()
{
  return (posL + posP)/2;
}
// rotacja obliczanie 
float rotation()
{
  return (posL - posP)/2;
}
// en_trans - translacja z enkoderow, set_trans - zadana translacja
float PD_translation()
{
  
  PWM_trans = Kp * error_trans + Kd*(error_trans - previous_error_trans);
  previous_error_trans = error_trans;

  return PWM_trans;
}
// en_rot - rotacja z enkoderow, set_rot - zadana rotacja
float PD_rotation()
{
  
  PWM_rot = Kp * error_rot + Kd*(error_rot - previous_error_rot);
  previous_error_rot = error_rot;

  return PWM_rot;
}

//generowanie sygnałów PWM, skręcanie robotem
void PWM_generator ()
{
  if (PWM_L<0)
  {
    PWM_L=abs(PWM_L);
    digitalWrite(kierunekLewe,LOW);  //USTAWIENIE KIERUNKU KOŁA LEWEGO W TYł    
  }
  else 
  {
    digitalWrite(kierunekLewe,HIGH); 
  }
  if (PWM_P<0)
  {
    PWM_P=abs(PWM_P);
    digitalWrite(kierunekPrawe,LOW);  //USTAWIENIE KIERUNKU KOŁA PRAWE W TYł    
  }
  else 
  {
    digitalWrite(kierunekPrawe,HIGH); 
  }
  analogWrite(predkoscLewe,PWM_L);  //ustawienie prędkości
  analogWrite(predkoscPrawe,PWM_P);  //
}


